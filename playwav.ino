/*
  Simple Audio Player for Arduino Zero

 Demonstrates the use of the Audio library for the Arduino Zero

 Hardware required :
 * Arduino shield with a SD card on CS4
 * A sound file named "test.wav" in the root directory of the SD card
 * An audio amplifier to connect to the DAC0 and ground
 * A speaker to connect to the audio amplifier

 
 Arturo Guadalupi <a.guadalupi@arduino.cc>
 Angelo Scialabba <a.scialabba@arduino.cc>
 Claudio Indellicati <c.indellicati@arduino.cc>

 This example code is in the public domain

 http://arduino.cc/en/Tutorial/SimpleAudioPlayerZero

*/

#include <SD.h>
#include <SPI.h>
#include <AudioZero.h>

void setup()
{
  // debug output at 115200 baud
  Serial.begin(9600);

  delay(3000);
  
  // setup SD-card
  Serial.println("Initializing SD card...");
//  if (!SD.begin(4)) {
  if (!SD.begin(SDCARD_SS_PIN)) {
    Serial.println(" failed!");
    while(true);
  }
  Serial.println(" done.");

  // 44100kHz stereo => 88200 sample rate
  AudioZero.begin(2*44100);

  SDFile nextFile;
}

void loop()
{
  int count = 0;

  SDFile file = SD.open("/");
  if (!file) {
    Serial.print("Couldn't open SD root");
  }

  SDFile nextFile;
  while (nextFile = file.openNextFile()) {
    const char* fileName = nextFile.name();
    Serial.print(fileName);
    int spaces = 25 - strlen(fileName); // Tabulate nicely
    while (spaces--) Serial.print(" ");
    Serial.print(nextFile.size()); Serial.println(" bytes");

    // open wave file from sdcard
    Serial.print("Playing... ");
 
    // until the file is not finished  
    AudioZero.play(nextFile);
  
    nextFile.close();
    Serial.println("End of file. Thank you for listening!");
  }  
}
